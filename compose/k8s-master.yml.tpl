etcd:
  image: gcr.io/google_containers/etcd:2.0.13
  net: host
  restart: always
  command:
    - /usr/local/bin/etcd
    - --bind-addr=0.0.0.0:4001
    - --data-dir=/var/etcd/data


apiserver:
  image: gcr.io/google_containers/hyperkube:v1.0.3
  net: host
  restart: always
  command:
    - /hyperkube
    - apiserver
    - --allow-privileged=true
    - --service-cluster-ip-range=172.17.0.0/24
    - --insecure-bind-address=0.0.0.0
    - --etcd_servers=http://localhost:4001
    - --cluster_name=kubernetes
    - --v=2


controller:
  image: gcr.io/google_containers/hyperkube:v1.0.3
  net: host
  restart: always
  command:
    - /hyperkube
    - controller-manager
    - --master=http://localhost:8080
    - --v=2


scheduler:
  image: gcr.io/google_containers/hyperkube:v1.0.3
  net: host
  restart: always
  command:
    - /hyperkube
    - scheduler
    - --master=http://localhost:8080
    - --v=2


kubelet:
  image: gcr.io/google_containers/hyperkube:v1.0.3
  net: host
  restart: always
  command:
    - /hyperkube
    - kubelet
    - --address=0.0.0.0
    - --enable_server
    - --api-servers=http://localhost:8080
    - --hostname-override=${MASTER}
    - --cadvisor-port=8080
    - --v=2
  volumes:
    - /var/run/docker.sock:/var/run/docker.sock


proxy:
  image: gcr.io/google_containers/hyperkube:v1.0.3
  net: host
  privileged: true
  restart: always
  command:
    - /hyperkube
    - proxy
    - --master=http://localhost:8080
    - --v=2
