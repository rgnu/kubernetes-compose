kubelet:
  image: gcr.io/google_containers/hyperkube:v1.0.3
  net: host
  restart: always
  command:
    - /hyperkube
    - kubelet
    - --address=0.0.0.0
    - --enable_server
    - --api-servers=http://${MASTER}:8080
    - --hostname-override=${NODE2}
    - --cadvisor-port=8080
    - --v=2
  volumes:
    - /var/run/docker.sock:/var/run/docker.sock


proxy:
  image: gcr.io/google_containers/hyperkube:v1.0.3
  net: host
  privileged: true
  restart: always
  command:
    - /hyperkube
    - proxy
    - --master=http://${MASTER}:8080
    - --v=2


cadvisor:
  image: google/cadvisor:latest
  net: host
  restart: always
  volumes:
    - /:/rootfs:ro
    - /var/run:/var/run:rw
    - /sys:/sys:ro
    - /var/lib/docker/:/var/lib/docker:ro
