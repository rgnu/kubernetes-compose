### Dependencies

- VirtualBox
- Vagrant

### Create Machines
    $ make node.start.01
    $ make node.start.02
    $ make node.start.03

### Setup Kubelet
    $ ./bin/kubectl config set-cluster default --server=http://192.168.98.101:8080/api/
    $ ./bin/kubectl config set-context default --cluster=default

### Test Cluster
    $ make kube.nodes

### Add Replication Controller
    $ make kube.create FILE=example/whoami-1.0.0.rc.yaml

### Add Service
    $ make kube.create FILE=example/whoami.svc.yaml

### Get Nodes,RC,Service and PODS
    $ make kube.nodes
    $ make kube.services
    $ make kube.rc
    $ make kube.pods

### Test whoami Service
    $ watch curl -s http://192.168.98.101:30003/

### Scale a Service to 4 replicas
    $ make kube.pods
    $ make kube.rc.scale.whoami-1.0.0 NUMBER=4
    $ make kube.pods

### Test whoami Service Again
    $ watch curl -s http://192.168.98.101:30003/

### Put a POD offline
    $ make kube.pods | awk '/whoami/{print "kube.pod.label."$2}' | head -1 | xargs make LABEL=status=online

### Rolling update
    $  make kube.rc.rolling-update.whoami-1.0.0 FILE=examples/whoami-2.0.0.rc.yaml

### Create a private registry
    $ make kube.create FILE=example/kube-registry.rc.yaml
    $ make kube.create FILE=example/kube-registry.svc.yaml
    $ docker run -d --name kube-registry-ambassador --expose 5000  -e REGISTRY_PORT_5000_TCP=tcp://192.168.98.101:30002 -p 5000:5000 svendowideit/ambassador

### Test  private registry
    $ docker pull busybox
    $ docker tag busybox localhost:5000/busybox
    $ docker push localhost:5000/busybox
