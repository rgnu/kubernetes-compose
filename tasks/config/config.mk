.SILENT:

APP_ENVIRONMENT?=production
APP_DIR?=$(CURDIR)
APP_BIN_DIR?=$(APP_DIR)/bin
APP_BUILD_DIR?=$(APP_DIR)/build
APP_TASK_DIR?=$(APP_DIR)/tasks
WGET?=/usr/bin/wget
ECHO?=echo
CURL?=curl
NOP?=$(ECHO) > /dev/null

include $(wildcard $(APP_TASK_DIR)/config/$(APP_ENVIRONMENT)/*.mk)
