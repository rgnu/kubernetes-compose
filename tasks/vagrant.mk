vagrant.%: VAGRANT?=vagrant


.PHONY: vagrant.ssh
DESCRIPTION+="vagrant.ssh: Log into node 1"
vagrant.ssh: vagrant.node.ssh.01


.PHONY: vagrant.node.ssh.%
DESCRIPTION+="vagrant.node.ssh.<NODE>: Log into node"
vagrant.node.ssh.%:
	cd $(CURDIR)/vagrant && $(VAGRANT) ssh kube-node$* -- -A


.PHONY: vagrant.node.start.%
DESCRIPTION+="vagrant.node.start.<NODE>: Start a new node"
vagrant.node.start.%:
	cd $(CURDIR)/vagrant && $(VAGRANT) up kube-node$*

.PHONY: vagrant.node.stop.%
DESCRIPTION+="vagrant.node.stop.<NODE>: Stop a node"
vagrant.node.stop.%:
	cd $(CURDIR)/vagrant && $(VAGRANT) halt kube-node$*

.PHONY: vagrant.node.rm.%
DESCRIPTION+="vagrant.node.rm.<NODE>: Remove a node"
vagrant.node.rm.%:
	cd $(CURDIR)/vagrant && $(VAGRANT) destroy kube-node$*
